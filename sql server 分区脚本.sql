Declare @DAY       INT =(DATEDIFF(DAY,Dateadd(Day,-34,GETDATE()),Dateadd(Day,-5,GETDATE())))+1;            -- 一共需要的分区
select @DAY
Declare @YEAR      Nvarchar(20)=(YEAR(GETDATE())),@MONTH Nvarchar(20)=(MONTH(GETDATE()));  -- 获取当前年月变量
Declare @NAME      Nvarchar(200);                                                          -- 存储文件组名称 分区方案名称变量 
Declare @FILEGROUP Nvarchar(MAX);                                                          -- 定义文件组变量
Declare @FILE      Nvarchar(MAX);                                                          -- 定义文件
Declare @FUN       Nvarchar(MAX), @FunValueStr Nvarchar(MAX)='',@project Nvarchar(MAX),@projectValue Nvarchar(MAX)='' ;
Declare @Location  Nvarchar(100)= 'C:\DataBase\';   
Declare @i int=1;                                      -- 文件组存放地址（手动修改）
--创建分区文件组 
While @DAY>0 
	Begin
	   Set @NAME ='TEST'+@YEAR+'_'+@MONTH+'_'+Cast(@DAY as Varchar);
	   Set @FILEGROUP='ALTER DATABASE [YY]  ADD FILEGROUP '+@NAME;
	   Set @DAY=@DAY-1;
	   --EXEC SP_EXECUTESQL @FILEGROUP;                                                     -- 执行动态sql语句插入文件组
	End	
--创建文件
SET @DAY=(DATEDIFF(DAY,Dateadd(Day,-30,GETDATE()),GETDATE()))+1;
While @DAY>0 
     Begin
	   Set @NAME ='TEST'+@YEAR+'_'+@MONTH+'_'+cast(@DAY as Varchar);
	   Set @FILE='ALTER DATABASE [YY]  ADD FILE(NAME=N'''+@NAME+''',FILENAME=N'''+@Location+@NAME+'.ndf'',SIZE=10MB, FILEGROWTH = 10% )TO FILEGROUP ['++@NAME+']';
	   Set @DAY=@DAY-1;
	   --EXEC SP_EXECUTESQL @FILE;
	 End
--创建分区函数
SET @DAY=(DATEDIFF(DAY,Dateadd(Day,-30,GETDATE()),GETDATE()));
While @i<=@DAY 
-- 获取创建分区函数地范围
     Begin
	 IF(@i>=10)
	    Set @NAME =@YEAR+'-'+@MONTH+'-'+cast(@i as varchar);               
	 Else
	    Set @NAME =@YEAR+'-'+@MONTH+'-'+LEFT('0'+Cast(@i as Nvarchar),2);

	 IF(@i=@DAY)
	     Set @FunValueStr=@FunValueStr+''''+@NAME+'''';
	 Else
	     Set @FunValueStr=@FunValueStr+''''+@NAME+''''+',';
	 Set @i=@i+1;
	 End
Set @FUN='CREATE PARTITION FUNCTION
	Fun_User_Id(DateTime) AS
	RANGE RIGHT
	FOR VALUES('+@FunValueStr+')';
    --EXEC SP_EXECUTESQL @FUN;
		print(@FUN)
--创建分区方案
Set @DAY=(DATEDIFF(DAY,Dateadd(Day,-30,GETDATE()),GETDATE()))+1;
set @i=1;
While @i<=@DAY  
-- 获取创建分区方案存储文件
    Begin
	   Set @NAME ='TEST'+@YEAR+'_'+@MONTH+'_'+cast(@i as Varchar);
	    IF(@i=@DAY)
	     Set @projectValue=@projectValue+@NAME;
	  Else
	     Set @projectValue=@projectValue+@NAME+','; 
	   Set @i=@i+1;
	End
Set @project='CREATE PARTITION SCHEME
	Sch_User_Id AS
	PARTITION Fun_User_Id
	TO('+@projectValue+')';
    --EXEC SP_EXECUTESQL @project;
	print(@project)
------------------------------------------------------------------------------------------------------分区表脚本
If Exists(Select Top 1 * from sysObjects where Id=OBJECT_ID(N'PrjInfo') and xtype='U')
 Begin
  Drop Table PrjInfo;
 End
  CREATE TABLE PrjInfo(
	[PrjID] [int] IDENTITY(1,1) NOT NULL,
	[FormID] [int] NOT NULL,
	[ClassID] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[BeginDate] [datetime] NULL,
	[FinishDate] [datetime] NULL,
	[LeadID] [int] NULL,
	[LeaderName] [nvarchar](50) NULL,
	[Content] [nvarchar](max) NOT NULL,
	[Teams] [nvarchar](500) NULL,
	[FinishCount] [int] NOT NULL,
	[Rate] [float] NULL,
	[BoardID] [int] NOT NULL,
	[PrjFee] [decimal](18, 2) NOT NULL,
	[CustomerName] [nvarchar](100) NULL,
	[CustomerAlias] [nvarchar](100) NULL,
	[ContractNo] [nvarchar](100) NULL,
	[IsVIP] [bit] NOT NULL,
	[Product] [nvarchar](50) NULL,
	[ProductStage] [nvarchar](50) NULL,
	[ProductStatus] [nvarchar](50) NULL,
	[Industry] [nvarchar](50) NULL,
	[BusinessMan] [nvarchar](50) NULL,
	[Implementer] [nvarchar](50) NULL,
	[IsAgent] [bit] NOT NULL,
	[Agent] [nvarchar](100) NULL,
	[InstallDate] [datetime] NULL,
	[OnlineDate] [datetime] NULL,
	[PlanAcceptDate] [datetime] NULL,
	[CurrentSituation] [nvarchar](500) NULL,
	[FocusProblem] [nvarchar](500) NULL,
	[ApplySupport] [nvarchar](50) NULL,
	[Note] [nvarchar](500) NULL,
	[HandOverNo] [nvarchar](100) NULL,
	[HandOverDate] [datetime] NULL
) ON Sch_User_Id(BeginDate)
-------------------------------------------------------------------------------------------------------插入数据
Declare @L INT=1
While @L>0
  Begin
	  --获取从2000至今的一个随机时间值为任务开始时间
	  DECLARE @start  datetime= '2020-09-28'; 
	  DECLARE @end    datetime=Getdate();
	  DECLARE @TIMEss datetime='2022-01-01 20:01:00.000';  --(Dateadd(Minute,Abs(Checksum(Newid()))%(Datediff(Minute,@start,@end)+1),@start));   -- 获取一个随机时间值
      INSERT INTO  PrjInfo VALUES
	               (0,0,2,'测试项目',@TIMEss,DATEADD(DAY,10,@TIMEss),4268,'邝嘉俊','测试','项目',0,0,1,50000,'北京协和医院','项目管理','CD000078',1,'医微云',
				    '售前试用','','政企','','',1,'测试','','','','','','','','FB0001245',GETDATE());
      SET @L=@L-1;
  End
SELECT * FROM PrjInfo
delete from PrjInfo
-------------------------------------------------------------------------------------------------------查找数据
Select $PARTITION.Fun_User_Id(BeginDate) as 分区编号,Count(1) as 记录数 From PrjInfo Group by $PARTITION.Fun_User_Id(BeginDate)


SELECT * FROM sys.partitions  WHERE OBJECT_NAME(OBJECT_ID)='PrjInfo'; 
Select * From PrjInfo Where $PARTITION.Fun_User_Id(BeginDate)=1
Union all
Select * From PrjInfo Where $PARTITION.Fun_User_Id(BeginDate)=2
Union all
Select * From PrjInfo Where $PARTITION.Fun_User_Id(BeginDate)=3
Union all
Select * From PrjInfo Where $PARTITION.Fun_User_Id(BeginDate)=4
Union all
Select * From PrjInfo Where $PARTITION.Fun_User_Id(BeginDate)=5
Union all
Select * From PrjInfo Where $PARTITION.Fun_User_Id(BeginDate)=6
Union all
Select * From PrjInfo Where $PARTITION.Fun_User_Id(BeginDate)=7
Union all
Select * From PrjInfo Where $PARTITION.Fun_User_Id(BeginDate)=8
Union all
Select * From PrjInfo Where $PARTITION.Fun_User_Id(BeginDate)=9
Union all
Select * From PrjInfo Where $PARTITION.Fun_User_Id(BeginDate)=10













