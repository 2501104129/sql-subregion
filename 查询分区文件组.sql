Declare @NAME      Nvarchar(200),@Value Nvarchar(200);  
Declare @FILEGROUP Nvarchar(MAX),@FILE      Nvarchar(MAX), @FUN   Nvarchar(MAX),@FangAn Nvarchar(MAX); 
Declare @Location  Nvarchar(100)= 'E:\DataBaseS\';   

Set @NAME ='TEST'+Cast(YEAR(GETDATE())as Varchar)+'_'+Cast(MONTH(GETDATE())as Varchar)+'_'+Cast(DAY(GETDATE()) as Varchar);
SET @NAME='TEST2021_12_2';
IF NOT EXISTS(SELECT 1 FROM sys.filegroups WHERE name=@NAME)AND NOT EXISTS(SELECT 1 FROM sys.database_files WHERE name =@NAME)
 Begin
    Set @FILEGROUP='ALTER DATABASE [YY]  ADD FILEGROUP '+@NAME;
	Set @FILE='ALTER DATABASE [YY]  ADD FILE(NAME=N'''+@NAME+''',FILENAME=N'''+@Location+@NAME+'.ndf'',SIZE=10MB, FILEGROWTH = 10% )TO FILEGROUP ['++@NAME+']';
	
	EXEC SP_EXECUTESQL @FILEGROUP;  
	EXEC SP_EXECUTESQL @FILE; 
	PRINT(@FILEGROUP);
	PRINT(@FILE);
	--添加分区函数分区方案
	SET @Value=Cast(YEAR(GETDATE())as Varchar)+'-'+Cast(MONTH(GETDATE())as Varchar)+'-'+Cast(DAY(GETDATE()) as Varchar);
	SET @Value='2021-12-02';
	SET @FUN='ALTER PARTITION FUNCTION  Fun_User_Id()  SPLIT RANGE ('''+@Value+''')';
	SET @FangAn='ALTER PARTITION SCHEME Sch_User_Id NEXT USED ['+@NAME+']';
    EXEC SP_EXECUTESQL @FangAn; 
	EXEC SP_EXECUTESQL @FUN;  

	PRINT(@FUN);
	PRINT(@FangAn);
 End
