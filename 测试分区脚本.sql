--创建分区函数
CREATE PARTITION FUNCTION partfunSale (datetime2)
AS RANGE RIGHT FOR VALUES ('2021-01-01','2021-02-01')
--创建一个分区方案
CREATE PARTITION SCHEME partschSalea
AS PARTITION partfunSale
TO (
 test2020,
 test2021,
 test2022)
--创建分区表
CREATE TABLE Sale(	
 [Id] [int] IDENTITY(1,1)  NOT NULL,
 [Name] [varchar](16) NOT NULL, 
 [SaleTime][datetime2]   NOT NULL
) ON partschSalea(SaleTime)
--drop table Sale
insert into Sale values('1','2020-01-12 00:17:00.000')
insert into Sale values('1','2021-01-12 00:17:00.000')
insert into Sale values('1','2021-01-12 00:17:00.000')
insert into Sale values('2','2021-01-11 00:17:00.000')
insert into Sale values('3','2021-02-11 00:17:00.000')
insert into Sale values('3','2021-03-13 00:17:00.000')
select * from Sale where $PARTITION.partfunSale(SaleTime)=1

select $PARTITION.partfunSale(SaleTime) as 分区编号,count(id) as 记录数 from Sale group by $PARTITION.partfunSale(SaleTime)


select * from Sale where $PARTITION.partfunSale(SaleTime)=1

