Declare @YEAR      Nvarchar(20)=(YEAR(GETDATE())),@MONTH Nvarchar(20)=(MONTH(GETDATE())),@DAY Nvarchar(20)=(Day(GETDATE()));  -- 获取当前年月变量
Declare @NAME      Nvarchar(200);                                                          -- 存储文件组名称 分区方案名称变量 
Declare @FILEGROUP Nvarchar(MAX);                                                          -- 定义文件组变量
Declare @FILE      Nvarchar(MAX);                                                          -- 定义文件
Declare @Location  Nvarchar(100)= 'C:\DataBase\';  
Declare @Value Nvarchar(200);     
Declare @FUN   Nvarchar(MAX),@FangAn Nvarchar(MAX);                                        --分区函数
 
 Begin  
begin transaction
  Set @NAME ='TEST'+@YEAR+'_'+@MONTH+'_'+Cast(@DAY as Varchar);
  Set @FILEGROUP='ALTER DATABASE [YY]  ADD FILEGROUP '+@NAME;                            --创建文件组

  Set @NAME ='TEST'+@YEAR+'_'+@MONTH+'_'+cast(@DAY as Varchar);                          --创建文件
  Set @FILE='ALTER DATABASE [YY]  ADD FILE(NAME=N'''+@NAME+''',FILENAME=N'''+@Location+@NAME+'.ndf'',SIZE=10MB, FILEGROWTH = 10% )TO FILEGROUP ['++@NAME+']';

  --修改执行分区函数分区方案
  SET @Value=Cast(@YEAR as Varchar)+'-'+Cast(@MONTH as Varchar)+'-'+Cast(@DAY as Varchar);
  SET @FUN='ALTER PARTITION FUNCTION  Fun_User_Id()  SPLIT RANGE ('''+@Value+''')';
  SET @FangAn='ALTER PARTITION SCHEME Sch_User_Id NEXT USED ['+@NAME+']';

  EXEC SP_EXECUTESQL @FILEGROUP; 
  EXEC SP_EXECUTESQL @FILE;   
  EXEC SP_EXECUTESQL @FangAn; 
  EXEC SP_EXECUTESQL @FUN; 
  --print(@FILEGROUP);
  --print(@FILE);
  --print(@FangAn);
  --print(@FUN);
commit transaction
 End